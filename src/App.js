import React from 'react'
import { hot } from 'react-hot-loader'

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>High Performance Web Development!</h1>
        <h3>Hey</h3>
      </div>
    )
  }
}

export default hot(module)(App)
